#!/usr/bin/env python
"""
Tests for src` package.
"""

import pytest
from click.testing import CliRunner

from file_manipulation import (
    ALL_LIBRARIES_FILE,
    ENCODING,
    EXTENSION,
    SECTIONS,
    SOURCE_FILE,
    get_downloading_file_list,
    get_tools_file_name,
    release_date_difference,
    release_difference,
)
from gitnventory import run


@pytest.fixture(name="get_file")
def get_file_name():
    """
    Return source file name
    """
    return f"../src/{SOURCE_FILE}"


@pytest.mark.parametrize(
    "command, expected_exit_code, expected_result",
    [
        (["--version"], 0, "GitVentory"),
        (["--help"], 0, "Show this message and exit."),
        (["--help"], 0, "--help"),
        (["--level", "NON_EXISTING", "--dry-run"], 2, "Invalid value for"),
        (["--NON_EXISTING_PARAMETER", "--dry-run"], 2, "No such option:"),
        (["--logging", "print", "--dry-run"], 1, ""),
    ],
)
def test_command_line_interface(command, expected_exit_code, expected_result):
    """Test the CLI."""
    runner = CliRunner()

    result = runner.invoke(run, command)

    assert result.exit_code == expected_exit_code
    assert expected_result in result.output


@pytest.mark.parametrize(
    "actual, expected",
    [
        (ALL_LIBRARIES_FILE, "all_libraries.csv"),
        (ENCODING, "utf-8"),
        (EXTENSION, "txt"),
        (SOURCE_FILE, "images_list.json"),
        (SECTIONS, ["images", "tools", "dbt_packages"]),
    ],
)
def test_static_data(actual, expected):
    """
    Test static data
    """
    assert actual == expected


def test_get_downloading_file_list(get_file):
    """
    Test file list
    """

    res = get_downloading_file_list(file_name=get_file, sections=SECTIONS)
    assert isinstance(res, list)
    assert res
    assert len(res) > 14

    for item in res:
        assert list(item.keys()) == ["URL", "file"]
        assert "https://" in item["URL"]


def test_get_tools_file_name(get_file):
    """
    test file names for tools
    """
    sections = ["dbt_packages", "tools"]
    actual = get_tools_file_name(sections=sections, file_name=get_file)

    for section, file_name in actual:
        assert section in sections
        assert ".json" in file_name


@pytest.mark.parametrize(
    "current, latest, expected",
    [
        ("1.0.0", "1.0.0", "Latest"),
        ("11.0.0", "11.0.0", "Latest"),
        ("1.0.0", "2.0.0", "Major"),
        ("1.1.0", "2.1.0", "Major"),
        ("0.0.1", "0.1.0", "Minor"),
        ("1.1.0", "1.2.0", "Minor"),
        ("1.1.5", "1.1.9", "Patch"),
        ("1.0.5", "1.0.9", "Patch"),
        ("1.0.5", "1.0.6", "Patch"),
        ("1.1", "12.0.6", "Major"),
        ("1..1.1.1", "12.0.6", "Invalid version"),
    ],
)
def test_release_difference(current, latest, expected):
    """
    Test release_difference
    """
    actual = release_difference(current_version=current, latest_version=latest)
    assert expected == actual


@pytest.mark.parametrize(
    "current, latest, expected",
    [
        ("2024-01-01", "2024-01-01", "0Y 0M"),
        ("2022-01-01", "2024-01-01", "2Y 0M"),
        ("2023-02-01", "2024-01-01", "0Y 11M"),
        ("2024-01-01", "2024-01-12", "0Y 0M"),
        ("2020-12-01", "2021-01-12", "0Y 1M"),
    ],
)
def test_release_date_difference(current, latest, expected):
    """
    Test release_date_difference
    """
    actual = release_date_difference(
        current_release_date=current, latest_release_date=latest
    )
    assert expected == actual
