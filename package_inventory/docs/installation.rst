.. highlight:: shell

============
Installation
============


Stable release
--------------

To install app, run this command in your terminal:

.. code-block:: console

    $ pip install app

This is the preferred method to install app, as it will always install the most recent stable release.

If you don't have `pip`_ installed, this `Python installation guide`_ can guide
you through the process.

.. _pip: https://pip.pypa.io
.. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/


From sources
------------

The sources for app can be downloaded from the `gitlab repo`_.

You can either clone the public repository:

.. code-block:: console

    $ git clone git://gitlab.com//app

Or download the `tarball`_:

.. code-block:: console

    $ curl -OJL https://gitlab.com//app/tarball/master

Once you have a copy of the source, you can install it with:

.. code-block:: console

    $ python setup.py install


.. _gitlab repo: https://gitlab.com//app
.. _tarball: https://gitlab.com//app/tarball/master
