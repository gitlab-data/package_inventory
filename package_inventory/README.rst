===
app
===




.. image:: https://gitlab.com//app/badges/main/pipeline.svg
        :target: https://gitlab.com//app/-/commits/main
        :alt: Pipeline Status

.. image:: https://gitlab.com//app/badges/main/coverage.svg
        :target: https://gitlab.com//app/-/commits/main
        :alt: Coverage report

.. image:: https://gitlab.com//app/-/badges/release.svg
        :target: https://gitlab.com//app/-/releases
        :alt: Latest Release

.. image:: https://gitlab.com//app/-/badges/main/pipeline.svg
        :target: https://.gitlab.io/app
        :alt: Documentation



GitLab Data Team package inventory list for Python, tools and libraries


* Free software: MIT license


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `gitlab-data/cookiecutter-pypackage`_, which was forked from the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://gitlab.com/audreyr/cookiecutter
.. _gitlab-data/cookiecutter-pypackage: https://gitlab.com/gitlab-data/cookiecutter-pypackage
.. _`audreyr/cookiecutter-pypackage`: https://gitlab.com/audreyr/cookiecutter-pypackage

