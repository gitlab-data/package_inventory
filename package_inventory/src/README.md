# Inventory list check

## Install environment

```bash
pip install -r requirements.txt
pip install pip-upgrader
```
* More information for [pip_upgrader](https://github.com/simion/pip-upgrader/tree/master/pip_upgrader).

## Setup

Image requirements URLs are located in the file `images_list.json` in the following format:

```json
{
    "data": [
        {
            "URL": "requirements_file_url",
            "file": "file_name_to_save"
        }
...
```

## Usage

* Run program
    ```bash
    # run file
    python3 gitnventory.py [--dry-run] [--logging [print|logging]] [--report_folder DEFINE_FOLDER] [--log_file DEFINE_LOG_FILE] [--level [DEBUG|INFO]]] [--help] [--markdown]
    ```

* Usage/help
    ```bash
    python3 gitnventory.py --help
    ```


### Options

* `--dry-run, -d`: If the argument is presence, program will do just a dry run without real execution
* `--logging [print/log], -l [print/logging]`: Default: `print`. Options: [`print`/`logging`] Choose the type of logging, either will print on console _(default value)_ or in the log file
* `--log_file, -lf `: Default: `generate_reports.log`. Specify destination for the log file
* `--report_folder -r`: Default: `report`. Choose the destination folder to save reports
* `--level, -lvl`: Default `DEBUG`. Choose logging option
* `--markdown, -md`: Default `False`. If the argument is presence, program will generate markdown report
* `--help`: call help and usage

## Result

**Note**: all files contains header, column separator is `,` and the line separator is `\n`.

What will be generated as the output of the program call:
1. **Get inventory list for each image** - - For utilities to check if the library we are implementing is outdated and how far away from the latest version. One file for each image analyzed, file will be saved in the format `rep_IMAGE_NAME_requirements.csv`. Showing all installed libraries. The following structure will be generated:
    * `LIBRARY` - library (package) name
    * `CURRENT_VERSION` - current version installed in the image
    * `CURRENT_VERSION_RELEASE_DATE` - when the version installed in the image was actually released. Date is represented in the format `YYYY-DD-MM`
    * `LATEST_VERSION` - the latest version (without pre-release) which is published
    * `LATEST_VERSION_RELEASE_DATE` - the release date of the latest released version.  Date is represented in the format `YYYY-DD-MM`
    Data example:
    ```csv
    LIBRARY, CURRENT_VERSION, CURRENT_VERSION_RELEASE_DATE, LATEST_VERSION, LATEST_VERSION_RELEASE_DATE
    astroid,2.11.3,2022-04-19,3.0.0,2023-09-26
    ```

1. **Check duplicated versions among images** Report with common libraries in image requirement files `rep_common_libraries.csv`. Listed only libraries where they are un use more than in one image. It has the following structure:
    * `LIBRARY` - library (package) name
    * `CNT` - number of appearances for the library in packages
    * `LISTED_IMAGE` - where the library is in use _(values are separated by `|`)_
    * `LISTED_VERSION` - which library versions are in use _(values are separated by `|`)_
    * `LATEST_INSTALLED` - latest installed version among images
    Data example:
    ```csv
    LIBRARY,CNT,LISTED_IMAGE,LISTED_VERSION,LATEST_INSTALLED
    Jinja2,2,airflow-image_requirements|analyst-image_requirements,3.1.2|3.1.2,3.1.2
    MarkupSafe,2,airflow-image_requirements|analyst-image_requirements,2.1.2|2.1.1,2.1.2
    ...
    ```
1. Check other tools latest version, ie. `dbt`, `airflow`... Report is located into `rep_tools.csv` file. It has the following structure:
    * `TOOLS` - name of tool
    * `LATEST_VERSION` - number of the latest version released
    * `RELEASE_DATE` - when the last version is released
    * `END_OF_LIFE` - when the support will be ended
1. `markdown_report.md` - in case the `--markdown` option is choosed, the app will generate fully prepared markdown report
