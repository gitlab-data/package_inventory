"""
Main module to check version usage for all GitLab Data Images
"""

import click
from loguru import logger

from analyse_images import analyze
from file_manipulation import (
    ALL_LIBRARIES_FILE,
    SOURCE_FILE,
    clean_up_temp_files,
    downloading,
    generate_markdown_report,
    generate_reports,
    global_var,
    set_basic_log_config,
)


@click.command(
    help="Application to check outdated packages for GitLab Data Team inventory."
)
@click.version_option("0.1.0", prog_name="GitVentory")
@click.option(
    "-d",
    "--dry-run",
    is_flag=True,
    default=False,
    show_default=True,
    help="If the argument is presence, program will do just a dry run without real execution",
)
@click.option(
    "-l",
    "--logging",
    type=click.Choice(["print", "logging"]),
    default="print",
    show_default=True,
    help="Choose the type of logging, either will print on console (default value) or in the log file",
)
@click.option(
    "-r",
    "--report_folder",
    type=click.STRING,
    default="reports",
    show_default=True,
    help="Choose the destination folder to save reports",
)
@click.option(
    "-lf",
    "--log_file",
    type=click.STRING,
    default="generate_reports.log",
    show_default=True,
    help="Specify destination for the log file",
)
@click.option(
    "-lvl",
    "--level",
    type=click.Choice(["INFO", "DEBUG"]),
    default="DEBUG",
    show_default=True,
    help="Choose the level of logging you want to print/log.",
)
@click.option(
    "-md",
    "--markdown",
    is_flag=True,
    default=False,
    show_default=True,
    help="If the argument is presence, program will generate markdown report",
)
def run(dry_run, logging, report_folder, log_file, level, markdown):
    """
    Main function to run the job
    """

    global_var.DRY_RUN = dry_run
    global_var.OUTPUT = logging
    global_var.REPORT_FOLDER = report_folder
    global_var.LOG_FILE = log_file
    global_var.LEVEL = level
    global_var.MARKDOWN = markdown

    set_basic_log_config(
        log_output=global_var.OUTPUT,
        log_file=global_var.LOG_FILE,
        level=global_var.LEVEL,
    )

    logger.success("Job started...")

    downloading(file_name=SOURCE_FILE)

    analyze(file_name=ALL_LIBRARIES_FILE)

    generate_reports(file_name=SOURCE_FILE)

    logger.success(f"--markdown {global_var.MARKDOWN}")

    if global_var.MARKDOWN:
        generate_markdown_report(report_folder=global_var.REPORT_FOLDER)

    clean_up_temp_files(file_name=SOURCE_FILE)

    logger.success("Successfully finished job")


if __name__ == "__main__":
    run()
