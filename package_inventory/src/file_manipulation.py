"""
Main routine to manipulate files
"""

import json
import os
import re
import subprocess
import sys
from collections import namedtuple
from datetime import datetime
from functools import lru_cache
from pathlib import Path
from typing import List

import requests
import semver
from dateutil import relativedelta
from loguru import logger

ALL_LIBRARIES_FILE = "all_libraries.csv"
SOURCE_FILE = "images_list.json"
EXTENSION = "txt"
ENCODING = "utf-8"
SECTIONS = ["images", "tools", "dbt_packages"]

global_var = namedtuple(
    "global_var", "DRY_RUN OUTPUT REPORT_FOLDER LOG_FILE LEVEL MARKDOWN"
)


def set_basic_log_config(log_output: str, log_file: str, level: str) -> None:
    """
    Set basic parameters for config
    """

    logger.remove(0)
    output = log_file

    if log_output == "print":
        output = sys.stderr

    logger.add(
        output,
        level=level,
        colorize=True,
        format=(
            "<green>{time:YYYY-MM-DD HH:mm:ss.SSS}</green> | "
            "<level>{level: <8}</level> | "
            "<cyan>{name}</cyan>:"
            "<cyan>{function}</cyan>:"
            "<cyan>{line}</cyan> - "
            "<level>{message}</level>"
        ),
    )


def delete_file(file_name: str) -> None:
    """
    Delete file, if exists
    """
    if global_var.DRY_RUN:
        logger.debug(f"delete file: {file_name}")
    else:
        if os.path.exists(file_name):
            os.remove(file_name)


def create_directory(full_path: str) -> None:
    """
    Create directory if it doesn't exist
    """

    if not os.path.exists(full_path):
        if global_var.DRY_RUN:
            logger.debug(f"Will create directory {full_path}")
        else:
            os.makedirs(full_path, exist_ok=True)


def read_from_file(file: str) -> str:
    """
    Read from file
    """
    if global_var.DRY_RUN:
        logger.debug(f"Read from file: {file}")
        return
    with open(file=file, mode="r", encoding=ENCODING) as report_file:
        for line in report_file:
            if (
                not line.startswith("[Dry Run]")
                and not line.startswith("Error while parsing package")
                and not line.startswith("Exception")
                and line.count(":") == 4
            ):
                yield line


def save_to_file(file: str, lines: list, header: str):
    """
    Save lines to files
    """
    if global_var.DRY_RUN:
        logger.debug(f"Save to file: {file}")
        return
    with open(file=file, mode="w", encoding=ENCODING) as report_file:
        if header:
            report_file.write(header + "\n")
        for line in lines:
            report_file.write(",".join(line) + "\n")


def run_shell_command(command: str) -> None:
    """
    Execute shel command
    """
    if global_var.DRY_RUN:
        logger.debug(f"run_shell_command: {command}")
        return

    subprocess.call(
        command, shell=True, stderr=subprocess.DEVNULL, stdout=subprocess.PIPE
    )


def get_downloading_file_list(file_name: str, sections: list) -> list:
    """
    Get all repos from images/tools we are using
    """
    res = []
    with open(file_name, "r", encoding=ENCODING) as json_file:
        repos = json.load(json_file)
        for section in sections:
            res.extend(repos.get(section))

    return res


@lru_cache(maxsize=256)
def get_release_date(package: str, version: str) -> str:
    """
    Get RELEASE_DATE column
    from RESTful API
    """
    url = "https://pypi.org/pypi"
    headers = {"Accept": "application/json"}
    try:
        result = requests.get(
            f"{url}/{package}/{version}/json", headers=headers, timeout=10
        )

        res = result.json()["urls"][0]["upload_time"][:10]
    except:
        res = "1970-01-01"

    return res


def get_clean_line(line: str) -> List[str]:
    """
    Clean up line
    """

    res = re.sub(r"\\:", "", line, 2).replace("...", "")
    res = re.sub("(\\()|(\\))|upgrade|available|uploaded on|==>", "", res)
    res = re.sub(r"\s+", " ", res)
    res = res.replace(" : ", " ")

    return res.split(" ")[1:-2]


def release_difference(current_version: str, latest_version: str) -> str:
    """
    Check difference in versions
    """
    if current_version == latest_version:
        return "Latest"

    try:
        current = semver.Version.parse(current_version, optional_minor_and_patch=True)
        latest = semver.Version.parse(latest_version, optional_minor_and_patch=True)

        if latest.major > current.major:
            return "Major"

        if latest.minor > current.minor:
            return "Minor"

        if latest.patch > current.patch:
            return "Patch"
    except ValueError as ve:
        return "Invalid version"


def release_date_difference(current_release_date: str, latest_release_date: str) -> str:
    """
    Difference between current release and the latest release
    in YYYY and MM
    """
    date_format = "%Y-%m-%d"
    start_date = datetime.strptime(current_release_date, date_format)
    end_date = datetime.strptime(latest_release_date, date_format)

    delta = relativedelta.relativedelta(end_date, start_date)
    return f"{delta.years}Y {delta.months}M"


def get_formatted_line(line: List[str]) -> List[str]:
    """
    format columns in proper order
    """
    FormattedLine = namedtuple(
        "FormattedLine",
        "library, current_version, current_version_release_date, latest_version, latest_version_release_date, release_difference, release_date_difference",
    )
    FormattedLine.library = line[0]
    FormattedLine.current_version = line[1]
    FormattedLine.current_version_release_date = get_release_date(
        package=FormattedLine.library, version=FormattedLine.current_version
    )
    FormattedLine.latest_version = line[2]
    FormattedLine.latest_version_release_date = line[3]
    FormattedLine.release_difference = release_difference(
        current_version=FormattedLine.current_version,
        latest_version=FormattedLine.latest_version,
    )
    FormattedLine.release_date_difference = release_date_difference(
        current_release_date=FormattedLine.current_version_release_date,
        latest_release_date=FormattedLine.latest_version_release_date,
    )
    res = [
        FormattedLine.library,
        FormattedLine.current_version,
        FormattedLine.current_version_release_date,
        FormattedLine.latest_version,
        FormattedLine.latest_version_release_date,
        FormattedLine.release_difference,
        FormattedLine.release_date_difference,
    ]
    return res


def prepare_cleaned_report(file: str):
    """
    Extract lines from report which one is generated
    """
    result = []
    for line in read_from_file(file=file):
        cleaned_line = get_clean_line(line=line)
        formatted_line = get_formatted_line(line=cleaned_line)

        result.append(formatted_line)

    header = "LIBRARY, CURRENT_VERSION, CURRENT_VERSION_RELEASE_DATE, LATEST_VERSION, LATEST_VERSION_RELEASE_DATE, RELEASE_DIFFERENCE, RELEASE_DATE_DIFFERENCE"

    target_file = file.replace(".txt", ".csv").replace("-", "_")
    save_to_file(file=target_file, lines=result, header=header)
    delete_file(file_name=file)


def download_files(repos: list) -> None:
    """
    Download requirements files from repos in a loop
    """

    for i, repo in enumerate(repos, start=1):
        url = repo.get("URL")
        file_to_save = repo.get("file")
        command = f"curl {url} > {file_to_save}"

        if global_var.DRY_RUN:
            logger.debug(f"... {i}/{len(repos)}) {command}")
        else:
            logger.debug(f"... {i}/{len(repos)})")
            delete_file(file_name=file_to_save)
            run_shell_command(command=command)


def downloading(file_name: str) -> None:
    """
    Process all packages and
    downloading files from the repos
    """
    logger.info(f"{'[dry-run]:' if global_var.DRY_RUN else ''} 1/4.. Downloading")

    repos = get_downloading_file_list(file_name=file_name, sections=SECTIONS)

    download_files(repos=repos)


def generate_reports(file_name: str) -> None:
    """
    Generate report using outdated lib
    """
    logger.info(f"{'[dry-run]:' if global_var.DRY_RUN else ''} 3/4 Generate report")

    full_path = f"{os.getcwd()}/{global_var.REPORT_FOLDER}"

    create_directory(full_path=full_path)

    repos = [
        repo.get("file")
        for repo in get_downloading_file_list(file_name=file_name, sections=["images"])
    ]

    for i, repo in enumerate(repos, start=1):
        report_file_name = f"{full_path}/rep_{repo}"
        logger.debug(f"...{i}/{len(repos)}. Generate report: {report_file_name}")

        command = f"echo q | pip-upgrade {repo} --dry-run -p all > {report_file_name}"

        run_shell_command(command=command)
        prepare_cleaned_report(file=report_file_name)


def clean_up_temp_files(file_name: str) -> None:
    """
    Clean up files after processing is done
    """
    logger.info(f"{'[dry-run]:' if global_var.DRY_RUN else ''} 4/4: Cleanup")

    repos = get_downloading_file_list(file_name=file_name, sections=SECTIONS)

    for repo in repos:
        file_to_delete = repo.get("file")
        delete_file(file_name=file_to_delete)

    delete_file(file_name=ALL_LIBRARIES_FILE)


def get_tools_file_name(sections: List[str], file_name) -> List[tuple]:
    """
    Get file names for processing
    """
    res = []
    with open(file_name, "r", encoding=ENCODING) as json_file:
        repos = json.load(json_file)

        for section in sections:
            res.extend([(section, x["file"]) for x in repos.get(section)])

    return res


def get_tools_prepared_row(section: str, file_name: str) -> list:
    """
    Get prepared row with the latest version
    """
    with open(file=file_name, mode="r", encoding=ENCODING) as json_file:
        file = json.load(json_file)

    row = file[0]
    res = []
    name = file_name.replace(".json", "")

    if section == "tools":
        res = [
            name,
            row.get("latest"),
            row.get("latestReleaseDate")[:10],
            str(row.get("eol", " ")).replace("False", " "),
        ]
    else:
        res = [
            name,
            row.get("name"),
            row.get("published_at")[:10],
            " ",
        ]
    return res


def get_report_list(folder: str) -> list:
    """
    Get file list
    """
    return [f for f in Path(folder).glob("*.csv") if f.is_file()]


def get_formatted_markdown_row(line: int, raw_line: str) -> str:
    """
    Format markdown table row
    """
    sep = "|"
    new_line = "\n"
    additional_column = f" Reason for upgrade {sep}"

    temp_line = raw_line.replace(",", "|").replace(new_line, "")

    prepared_line = f"{sep}{temp_line}{sep}"

    res = prepared_line

    if line == 1:
        res = f"{res.replace(new_line, '')} {additional_column}"
        below_header = f"{sep}".join(["----" for x in range(prepared_line.count(sep))])
        below_header = f"\n{sep}{below_header}{sep}\n"

        res += below_header
    else:
        res = f"{res} {sep}"
        res += new_line
    return res


def generate_header(no: int, file_name: str) -> str:
    """
    Generate markdown header
    """
    empty_line = ""
    new_line = "\n"
    res = ""

    if no == 1:
        res = f"# Report for the package manager{new_line}{empty_line}{new_line}"
    else:
        report_title = file_name.replace(global_var.REPORT_FOLDER + "/", "").replace(
            ".csv", ""
        )
        res = f"{new_line}{empty_line}{new_line} ## {report_title}{new_line}{empty_line}{new_line}"
    return res


def format_markdown_line(no: int, raw_line: str, file: str) -> str:
    """
    Format special case to replace | with > and after formatting
    revert back to ,
    """
    res = raw_line
    if file.endswith("rep_common_libraries.csv"):
        res = res.replace("|", ">")

    res = get_formatted_markdown_row(line=no, raw_line=res).replace(">", ",")

    return res


def write_to_markdown_file(files: list, target_file) -> None:
    """
    Write into markdown report via list of files
    """
    target_file.write(generate_header(no=1, file_name=""))

    for file in files:
        with open(file=file, mode="r", encoding=ENCODING) as lines:
            target_file.write(generate_header(no=2, file_name=str(file)))

            for i, line in enumerate(lines, start=1):
                formatted_line = format_markdown_line(
                    no=i, raw_line=line, file=str(file)
                )
                target_file.write(formatted_line)


def generate_markdown_report(report_folder: str) -> None:
    """
    Generate markdown file
    """
    logger.success("Starting generating markdown report")

    file_list = get_report_list(folder=report_folder)

    markdown_report = f"{global_var.REPORT_FOLDER}/markdown_report.md"

    if global_var.DRY_RUN:
        logger.debug(f"Generate markdown report {markdown_report}")
    else:
        with open(markdown_report, mode="w", encoding=ENCODING) as markdown_report:
            write_to_markdown_file(files=file_list, target_file=markdown_report)

    logger.success("Done with generating markdown report")
