"""
Module to analyze requirements.txt files
"""

import glob

import pandas as pd
from loguru import logger

from file_manipulation import (
    ALL_LIBRARIES_FILE,
    ENCODING,
    EXTENSION,
    SOURCE_FILE,
    create_directory,
    delete_file,
    get_tools_file_name,
    get_tools_prepared_row,
    global_var,
)


def save_libraries_to_file(src_file: str, lib_list: list) -> None:
    """
    Save to file
    """
    library_list_completed = []
    library_list_completed.extend(lib_list)

    with open(ALL_LIBRARIES_FILE, mode="a+", encoding=ENCODING) as w:
        for line in lib_list:
            quoted = (
                '"'
                + src_file.replace("." + EXTENSION, "")
                + '","'
                + line.replace("\n", "")
                + '"'
                + "\n"
            )

            w.writelines(f"{quoted}")


def generate_libraries(extension: str) -> None:
    """
    generate files with list of libraries
    """
    file_list = glob.glob(f"*.{extension}")

    for file in file_list:
        if global_var.DRY_RUN:
            logger.debug(f"generate {file}")
        else:
            if file != "requirements.txt":  # exclude main project level file
                with open(file, mode="r", encoding=ENCODING) as f:
                    lines = f.readlines()

                save_libraries_to_file(src_file=file, lib_list=lines)


def get_prepared_lines(file_name: str) -> list:
    """
    Return prepared line for processing
    """
    res = []
    with open(file_name, mode="r", encoding=ENCODING) as read_libraries:
        for line in read_libraries:
            line_separated = line.replace("\n", "").replace('"', "").split(",")

            image = line_separated[0]
            library_value = line_separated[1]

            if not library_value[0] == "#":
                if "==" in library_value:
                    library_index = library_value.index("==")
                else:
                    library_index = library_value.index(">=")

                library = library_value[:library_index]
                version = library_value[library_index + 2 :]

                res.append([image, library, version])

    return sorted(res, key=lambda x: x[1], reverse=False)


def load_dataframe(input_list: list) -> pd.DataFrame:
    """
    Return loaded DataFrame from a list
    """
    return pd.DataFrame(input_list, columns=["image", "library", "version"])


def group_concat(
    input_data_frame: pd.DataFrame, group_by_column: str, agg_column: str
) -> pd.DataFrame:
    """
    Simulate group_concat function using
    DataFrame commands
    """
    delimiter = "|"

    return input_data_frame.groupby(group_by_column).agg(
        {agg_column: lambda x: delimiter.join(x)}
    )


def analyze_libraries(src_file: str) -> None:
    """
    Do analysis with all libraries
    """
    if global_var.DRY_RUN:
        logger.debug(f"Generate Pandas DataFrame for the source file: {src_file}")
        return

    create_directory(full_path=global_var.REPORT_FOLDER)

    lines = get_prepared_lines(file_name=ALL_LIBRARIES_FILE)

    df = load_dataframe(input_list=lines)

    df_image_group_concat = group_concat(
        input_data_frame=df, group_by_column="library", agg_column="image"
    )
    df_image_group_concat.rename(columns={"image": "listed_image"}, inplace=True)

    df_version_group_concat = group_concat(
        input_data_frame=df, group_by_column="library", agg_column="version"
    )
    df_version_group_concat.rename(columns={"image": "listed_version"}, inplace=True)

    df_count = df.groupby(["library"])["image"].count()
    df_max = df.groupby("library").agg({"version": "max"})

    df = pd.merge(
        df_image_group_concat, df_version_group_concat, on="library", how="outer"
    )
    df = pd.merge(df, df_count, on="library", how="outer")
    df = pd.merge(df, df_max, on="library", how="outer")

    df.rename(
        columns={
            "image": "count",
            "version_x": "listed_version",
            "version_y": "latest_installed",
        },
        inplace=True,
    )
    df = df[["count", "listed_image", "listed_version", "latest_installed"]]
    df = df.query("count > 1").sort_values(by=["library"], ascending=True)

    df.to_csv(
        f"{global_var.REPORT_FOLDER}/rep_common_libraries.csv",
        sep=",",
        encoding=ENCODING,
    )


def save_tools_to_file(file_name: str, rows: list) -> None:
    """
    Save result to file
    """
    df = pd.DataFrame(data=rows)
    df.columns = ["tools", "latest_version", "release_date", "end_of_life"]

    df.to_csv(
        f"{global_var.REPORT_FOLDER}/{file_name}.csv",
        sep=",",
        encoding=ENCODING,
        index=False,
    )


def analize_tools(src_file: str) -> None:
    """
    Analyze tools
    """
    create_directory(full_path=global_var.REPORT_FOLDER)

    logger.debug("Starting analyzing tools")

    files = get_tools_file_name(sections=["tools", "dbt_packages"], file_name=src_file)
    prepared_rows = []

    tools_final_file = "rep_tools"

    for section, file in files:
        if global_var.DRY_RUN:
            logger.debug(f"Tools: generate data for file: {file}")
        else:
            prepared_rows.append(
                get_tools_prepared_row(section=section, file_name=file)
            )
    logger.debug(f"save prepared data for tools in the file: {tools_final_file}")
    if global_var.DRY_RUN:
        return
    save_tools_to_file(file_name=tools_final_file, rows=prepared_rows)


def analyze(file_name: str) -> None:
    """
    Analyze libraries we use
    """
    logger.info(f"{'[dry-run]:' if global_var.DRY_RUN else ''} 2/4: Analyze")

    delete_file(file_name=ALL_LIBRARIES_FILE)

    generate_libraries(extension=EXTENSION)

    analyze_libraries(src_file=file_name)

    analize_tools(src_file=SOURCE_FILE)
